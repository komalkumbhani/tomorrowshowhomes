<?php

global $project;
$project = 'mysite';

global $databaseConfig;
$databaseConfig = array(
	'type' => 'MySQLDatabase',
	'server' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'SilverStripe',
	'path' => ''
);

// Set the site locale
i18n::set_locale('en_US');

// enable fulltext search
FulltextSearchable::enable();

// set valid styles for the text editor field in CMS
HtmlEditorConfig::get('cms')->setOption(
    'extended_valid_elements',
    'img[class|src|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name|usemap],' .
    'iframe[src|name|width|height|title|align|allowfullscreen|frameborder|marginwidth|marginheight|scrolling],' .
    'object[classid|codebase|width|height|data|type],' .
    'embed[src|type|pluginspage|width|height|autoplay],' .
    'param[name|value],' .
    'map[class|name|id],' .
    'area[shape|coords|href|target|alt],' .
    'ol[start|type]'
);

HtmlEditorConfig::get('cms')->enablePlugins(array('myplugin' => '../../../mysite/javascript/myplugin/editor_plugin.js'));
HtmlEditorConfig::get('cms')->setOption('element_format', 'html');

HtmlEditorConfig::get('cms')->enablePlugins('spellchecker', 'contextmenu');
HtmlEditorConfig::get('cms')->addButtonsToLine(2, 'spellchecker');
HtmlEditorConfig::get('cms')->setOption(
    'spellchecker_rpc_url', 
    THIRDPARTY_DIR . '/tinymce-spellchecker/rpc.php'
);
HtmlEditorConfig::get('cms')->setOption('browser_spellcheck', false);
//HtmlEditorConfig::get('cwp')->disablePlugin('media');
HtmlEditorConfig::get('cwp')->setOption('extended_valid_elements', '<your modified whitelist goes here>');

//google MAP API key
define("GOOGLE_MAP_KEY", "AIzaSyBz-dr8tyIcRJZTwPbrOr0Ry95KXSX4qVU");