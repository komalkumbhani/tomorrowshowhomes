(function() {
    tinymce.create('tinymce.plugins.myplugin', {

        init : function(ed, url) {
            var self = this;

            ed.addButton ('myplugin', {
                'title' : 'My plugin',
                'image' : url+'/myplugin.png',
                'onclick' : function () {
                    alert('Congratulations! Your plugin works!');
                }
            });

        },

        getInfo : function() {
            return {
                longname  : 'myplugin',
                author      : 'Me',
                authorurl : 'http://me.org.nz/',
                infourl   : 'http://me.org.nz/myplugin/',
                version   : "1.0"
            };
        }
    });

    tinymce.PluginManager.add('myplugin', tinymce.plugins.myplugin);
})();