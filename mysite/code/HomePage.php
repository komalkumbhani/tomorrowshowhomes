<?php
class HomePage extends Page{

}

class HomePage_Controller extends Page_Controller{

	public function FeaturedProperties() {
	    return Property::get()
		    ->filter(array(
		    	'FeaturedOnHomepage' => false
		    ))
		    ->limit(3);
	}

	public function Featured_Banner() {
	    return Property::get()
		    ->filter(array(
		    	'FeaturedOnHomepage' => true
		    ))
		    ->limit(3);
	}	
	

}