<?php
class Agent extends DataObject {

    private static $db = array (
        'Name' => 'varchar',
        'Description' => 'varchar',
        'Phone' => 'int',
        'Email' => 'varchar',
        'Mobile' => 'int',
        'Skype' => 'varchar'        
   );

    private static $has_one = array (
        'PrimaryPhoto' => 'Image'
    );

    private static $summary_fields = array (
        'Name' => 'Agent Name',
        'Description' => 'Description',
        'Phone' => 'Phone',
        'Email' => 'Email',
        'Mobile' => 'Mobile',
        'Skype' => 'Skype'
    );

    private static $searchable_fields = array (
        'Name',
        'Email'
    );

    public function searchableFields() {
        return array (
            'Name' => array (
                'filter' => 'PartialMatchFilter',
                'title' => 'Name',
                'field' => 'TextField'
            ),
            'Email' => array (
                'filter' => 'ExactMatchFilter',
                'title' => 'Email',
                'field' => 'TextField'              
            )
        );
    }

    public function getCMSfields() {
            $fields = parent::getCMSFields();
            $fields = FieldList::create(TabSet::create('Root'));
            $fields->addFieldsToTab('Root.Main', array(
                TextField::create('Name'),
                TextareaField::create('Description'),
                TextField::create('Phone'),
                TextField::create('Email'),
                TextField::create('Mobile'),
                TextField::create('Skype')
        ));
         
        $fields->addFieldToTab('Root.Image', $upload = UploadField::create(
            'PrimaryPhoto',
            'Primary photo'
        ));

        $upload->getValidator()->setAllowedExtensions(array(
            'png','jpeg','jpg','gif'
        ));
        $upload->setFolderName('agent-photos');

        return $fields;
    }
}