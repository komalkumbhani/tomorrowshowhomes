<?php
class SuburbPage extends Page{

}

class SuburbPage_Controller extends Page_Controller{
	public function LastArticles( $count = 3 ){
		return ArtilcePage::get()
				->sort('Created','DESC')
				->limit($count);
	}

	public function SuburbCode() {
	    return Suburb::get()
		    ->filter(array(
		        'SuburbName' => true
		    ))
		    ->limit(30);
	}
}