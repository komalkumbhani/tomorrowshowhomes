<?php
class CountryAdmin extends ModelAdmin {

    private static $menu_title = 'Country';

    private static $url_segment = 'Country';

    private static $managed_models = array (
        'Country'
    );

    //private static $menu_icon = 'mysite/icons/property.png';
}