<?php
class SimilarPropertiesPage extends Page{

}

class SimilarPropertiesPage_Controller extends Page_Controller{

	public function SimilarProperties() {
	    return SimilarProperties::get()
		    ->filter(array(
		        'S_PropertyName' => true
		    ))
		    ->limit(30);
	}
}