<?php
class Page extends SiteTree {

	private static $db = array();

	private static $has_one = array();

}
class Page_Controller extends ContentController {

	private static $allowed_actions = array ();
	
	public function init() {
		parent::init();
	}

	public function index(SS_HTTPRequest $request) {	

	    if($request->isAjax()) {
	        return "Ajax response!"; 
	    }

	    return array (
	        'Results' => $paginatedProperties
	    );
	}

	public function getCountryNameByID($id){
		$data = Country::get()->Filter("ID", $id);
		$name = '';
		foreach( $data as $k ){
			$name = $k->CountryName;
		}
		return $name;
	}

	public function getDistrictNameByID($id){
		$data = District::get()->Filter("ID", $id);
		$name = '';
		foreach( $data as $k ){
			$name = $k->DistrictName;
		}
		return $name;
	}

	public function getCountryNames(){
		$country = Country::get();
		$list = new ArrayList();
		$array = array("Country" => $country,
						"Selected" => $_REQUEST['form-sale-country']
	                    );

		$list->push($array);
		return $list;
	}

	public function getDistrictName(){
		$district = District::get();
		$list = new ArrayList();
		$array = array("District" => $district,
						"Selected" => $_REQUEST['form-sale-district']
	                    );

		$list->push($array);
	return $list;
	}

	public function getSuburbName(){
		$suburb = Suburb::get();
		$list = new ArrayList();
		$array = array("Suburb" => $suburb,
						"Selected" => $_REQUEST['form-sale-suburb']
	                    );

		$list->push($array);
	return $list;
	}

	public function getDistrictNames(){
		return District::get();
	}

	public function getSuburbNames(){
		return Suburb::get();
	}

	public function getBedrooms(){
		return Property::get();
	}

	public function getBathrooms(){
		return Property::get();
	}



	public function getSearchParams(){
		$list = new ArrayList();
		if( !empty($_REQUEST) ){
			$array = array("CountrySearch" => $_REQUEST['form-sale-country'],
	                       "DistrictSearch" => $_REQUEST['form-sale-district'],
	                       "SuburbSearch" => $_REQUEST['form-sale-suburb'],
                           "Bedrooms_from" => $_REQUEST['form-sale-bedrooms-from'],
                           "Bedrooms_to" => $_REQUEST['form-sale-bedrooms-to'],
                           "Bathrooms_from" => $_REQUEST['form-sale-bathrooms-from'],
                           "Bathrooms_to" => $_REQUEST['form-sale-bathrooms-to']
	                    );
			$list->push($array);
		}
		//echo "<pre>";print_r($list); echo "</pre>";
		return $list;
	}

	public function getTommorrows_Show_Homes( $count = 9 ){
		
		$params = array();
    	if($this->getRequest()->getVar('form-sale-country') != ''){
    		$params['CountryName'] = $this->getRequest()->getVar('form-sale-country');
    	}
    	if($this->getRequest()->getVar('form-sale-district') != ''){
    		$params['DistrictName'] = $this->getRequest()->getVar('form-sale-district');
    	}
    	if($this->getRequest()->getVar('form-sale-suburb') != ''){
    		$params['SuburbName'] = $this->getRequest()->getVar('form-sale-suburb');
    	}
    	
    	if($this->getRequest()->getVar('form-sale-bedrooms-from') != '' ){
    		$params['Bedrooms:GreaterThanOrEqual'] = $this->getRequest()->getVar('form-sale-bedrooms-from');    		
    	}
    	if( $this->getRequest()->getVar('form-sale-bedrooms-to') != ''){
    		$params['Bedrooms:LessThanOrEqual'] = $this->getRequest()->getVar('form-sale-bedrooms-to');
    	}
    	if($this->getRequest()->getVar('form-sale-bathrooms-from') != '' ){
    		$params['Bathrooms:GreaterThanOrEqual'] = $this->getRequest()->getVar('form-sale-bathrooms-from');    		
    	}
    	if( $this->getRequest()->getVar('form-sale-bathrooms-to') != ''){    	
    		$params['Bathrooms:LessThanOrEqual'] = $this->getRequest()->getVar('form-sale-bathrooms-to');
    	}
    	//echo "<pre>";print_r($params); die();
	 	$sql = Property::get()
			->filter($params)
			->sort('Created','DESC')
			->limit($count);
		return $sql;		
	}

	public function getYourProperties( $count = 6 ) {
	    
	    $params = array();
    	if($this->getRequest()->getVar('form-sale-country') != ''){
    		$params['CountryName'] = $this->getRequest()->getVar('form-sale-country');
    	}
    	if($this->getRequest()->getVar('form-sale-district') != ''){
    		$params['DistrictName'] = $this->getRequest()->getVar('form-sale-district');
    	}
    	if($this->getRequest()->getVar('form-sale-suburb') != ''){
    		$params['SuburbName'] = $this->getRequest()->getVar('form-sale-suburb');
    	}
    	
    	if($this->getRequest()->getVar('form-sale-bedrooms-from') != '' ){
    		$params['Bedrooms:GreaterThanOrEqual'] = $this->getRequest()->getVar('form-sale-bedrooms-from');    		
    	}
    	if( $this->getRequest()->getVar('form-sale-bedrooms-to') != ''){
    		$params['Bedrooms:LessThanOrEqual'] = $this->getRequest()->getVar('form-sale-bedrooms-to');
    	}
    	if($this->getRequest()->getVar('form-sale-bathrooms-from') != '' ){
    		$params['Bathrooms:GreaterThanOrEqual'] = $this->getRequest()->getVar('form-sale-bathrooms-from');    		
    	}
    	if( $this->getRequest()->getVar('form-sale-bathrooms-to') != ''){    	
    		$params['Bathrooms:LessThanOrEqual'] = $this->getRequest()->getVar('form-sale-bathrooms-to');
    	}
    	//echo "<pre>";print_r($params); die();
	 	$sql = Property::get()
			->filter($params)
			->sort('Created','DESC')
			->limit($count);
		return $sql;
	}
	
	public function isSearch() { 
    	if( !empty($_REQUEST['form-sale-country']) || !empty($_REQUEST['form-sale-district']) || !empty($_REQUEST['form-sale-suburb']) || !empty($_REQUEST['form-sale-bedrooms-from']) || !empty($_REQUEST['form-sale-bedrooms-to']) || !empty($_REQUEST['form-sale-bathrooms-from']) || !empty($_REQUEST['form-sale-bathrooms-to']) ){    		
    		return 1;    		
    	}else{
    		return 0;
    	} }

	public function getAboutusDetails(){
		return SiteTree::get()->filter('ID','2');
	}

}