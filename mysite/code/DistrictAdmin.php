<?php
class DistrictAdmin extends ModelAdmin {

    private static $menu_title = 'District';

    private static $url_segment = 'District';

    private static $managed_models = array (
        'District'
    );

    //private static $menu_icon = 'mysite/icons/property.png';
}