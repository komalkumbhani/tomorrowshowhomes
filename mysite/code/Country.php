<?php
class Country extends DataObject {

    private static $db = array (
        'CountryName' => 'Varchar'
    );

    private static $summary_fields = array (
        'CountryName' => 'Country Name'
    );

    private static $searchable_fields = array (
        'CountryName'
    );

    public function searchableFields() {
        return array (
            'CountryName' => array (
                'filter' => 'ExactMatchFilter',
                'title' => 'Country Name',
                'field' => DropdownField::create('CountryName')
                                ->setSource(ArrayLib::valuekey(range(1,10)))
                                ->setEmptyString('-- Any Country --')
            )
        );
    }

    public function getCMSfields() {
        $fields = FieldList::create(TabSet::create('Root'));
        $fields->addFieldsToTab('Root.Country', array(
            TextField::create('CountryName')
            //DropdownField::create('CountryName','Country Name',Country::get()->map('ID','CountryName'))
                //->setEmptyString('--Select a Country--')
        ));

    return $fields;
    }
}