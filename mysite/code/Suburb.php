<?php
class Suburb extends DataObject {

    private static $db = array (
        'CountryID' => 'int',
        'DistrictID' => 'int',
        'SuburbName' => 'Varchar'
    );

    private static $summary_fields = array (
        'SuburbName' => 'Suburb Name',
        'CountryID' => 'Country Name',
        'DistrictID' => 'District Name'        
    );

    private static $searchable_fields = array (
        'SuburbName'
    );

    public function searchableFields() {
        return array (
            'SuburbName' => array (
                'filter' => 'ExactMatchFilter',
                'title' => 'Suburb Name',
                'field' => DropdownField::create('SuburbName')
                                ->setSource(ArrayLib::valuekey(range(1,10)))
                                ->setEmptyString('-- Any Suburb --'),
            )
        );
    }

    function getCountry_Name() {
        $newID = $ID;
        return Country::get_one("CountryName", "CountryID == $newID");
        //return Country::get()->filter('CountryID',$this->ID);
    }

    function getDistrict_Name() {
        $dis_ID = $ID;
        return District::get_one("DistrictName", "DistrictID == $dis_ID");
        //return District::get()->filter('DistrictID',$this->ID);
    }
    public function getCMSfields() {
        $fields = FieldList::create(TabSet::create('Root'));
        $fields->addFieldsToTab('Root.Suburb', array(
            DropdownField::create('CountryID','Country Name',Country::get()->map('ID','CountryName'))
                ->setEmptyString('--Select a Country--'),
            DropdownField::create('DistrictID','District Name',District::get()->map('ID','DistrictName'))
                ->setEmptyString('--Select a District--'),
            textField::create('SuburbName')
            //DropdownField::create('SuburbName','Suburb Name',Suburb::get()->map('ID','SuburbName'))
                //->setEmptyString('--Select a Suburb--')
        ));

    return $fields;
    }
}