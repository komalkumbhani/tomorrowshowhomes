<?php
class DistrictPage extends Page{

}

class DistrictPage_Controller extends Page_Controller{
	public function LastArticles( $count = 3 ){
		return ArtilcePage::get()
				->sort('Created','DESC')
				->limit($count);
	}

	public function DistrictCode() {
	    return District::get()
		    ->filter(array(
		        'CountryName' => true
		    ))
		    ->limit(30);
	}
}