<?php
class SimilarProperties extends DataObject {

    private static $db = array (
        'S_PropertyName' => 'Varchar'
    );

    private static $summary_fields = array (
        'S_PropertyName' => 'Title'
    );
    
    public function getCMSfields() {
        $fields = FieldList::create(TabSet::create('Root'));
        $fields->addFieldsToTab('Root.Main', array(
        DropdownField::create('S_PropertyName','Related Property',Property::get()->map('Title','Title'))
                ->setEmptyString('--Select a Similar Property--') 
        ));
    return $fields;
    }
}