<?php
class District extends DataObject {

    private static $db = array (
        'CountryID' => 'int',
        'DistrictName' => 'Varchar'
        //'CountryName' => 'Varchar'
    );

    private static $summary_fields = array (
        'DistrictName' => 'District Name',
        'CountryID' => 'Country Name'
    );

    private static $searchable_fields = array (
        'DistrictName'
    );

    public function searchableFields() {
        return array (
            'DistrictName' => array (
                'filter' => 'ExactMatchFilter',
                'title' => 'District Name',
                'field' => DropdownField::create('DistrictName')
                                ->setSource(ArrayLib::valuekey(range(1,10)))
                                ->setEmptyString('-- Any District --')
            )
        );
    }

    public function getCMSfields() {
        $fields = parent::getCMSFields();
        //$C_Name = Country::get('CountryName')->byID($this->CountryID);
        $fields = FieldList::create(TabSet::create('Root'));
        $fields->addFieldsToTab('Root.District', array(
            DropdownField::create('CountryID','Country Name',Country::get()->map('ID','CountryName'))
                ->setEmptyString('--Select a Country--'),
            TextField::create('DistrictName')
            //HiddenField::create('CountryID',$C_Name)
        ));

    return $fields;
    }

    /*function getCountries() {
            return Country::get()
                ->where("\"ID\" = " . $this->CountryID);
    }*/
}