<?php
class AgentAdmin extends ModelAdmin {

    private static $menu_title = 'Agent';

    private static $url_segment = 'agent';

    private static $managed_models = array (
        'agent'
    );

    private static $menu_icon = 'mysite/icons/user_icon.png';
}