<?php
class PropertyDetailPage extends Page{

}

class PropertyDetailPage_Controller extends Page_Controller{
	
	public function getYouTubeCode()
    {
    	$property_id = $_REQUEST['id'];
    	$code = '';
    	if( !empty($property_id) ){    		
	        $data = Property::get()->Filter('ID', $property_id);
	        foreach($data as $k ){
	        	$code = $k->VideoFile;
	        }
	        $query = parse_url($code);

	        if (isset($query['query'])) {
	            $query = $query['query'];
	            parse_str($query, $vars);
	            return $vars['v'];
	        }
	    }
        return $code;
    }	

	public function getSingleProperty() {
		$id = $_REQUEST['id'];
		$data = Property::get()->filter('ID',$id);
		return $data;
	}
	
	public function getAgent() {
		$id = $_REQUEST['id'];		
		$Properties = Property::get()->Filter('ID', $id);
		$list = new ArrayList();
		foreach($Properties as $Property) {
		    $Agents = Agent::get()->filter(array(
		        'ID' => $Property->AgentID
		    ));
		    foreach($Agents as $agent) {
				$array = array("Title" => $Property->Title,
			                    "Description" => $agent->Description,
			                    "Name" => $agent->Name,
			                    "AgentDetails" => $agent);
			    $list->push($array);
			}	    
		}
		return $list;
	}

	public function getCurrentURL(){
		$url = trim($_SERVER['REQUEST_URI'],"/SilverStripe/");
		return $url;
	}

	public function FeaturedProperties(){
		$id = $_REQUEST['id'];
		$data = Property::get()->byID($id);		
		$list = new ArrayList();
		$related_ids_data = array();
		$FeaturedProperties = '';
		foreach( $data as $k ){
			$related_prop_ids = explode(',',$k->Related_properties);
			//print_r($related_prop_ids); exit;
			if(count($related_prop_ids) > 0){
				foreach($related_prop_ids as $rid){
					$FeaturedProperties = Property::get()->byID($rid);					
					$list->push($FeaturedProperties);
				}
			}
		}
		return $list;
	}
}