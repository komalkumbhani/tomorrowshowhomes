<?php
class CountryPage extends Page{

}

class CountryPage_Controller extends Page_Controller{
	public function LastArticles( $count = 3 ){
		return ArtilcePage::get()
				->sort('Created','DESC')
				->limit($count);
	}

	public function CountryCode() {
	    return Country::get()
		    ->filter(array(
		        'CountryName' => true
		    ))
		    ->limit(30);
	}
}