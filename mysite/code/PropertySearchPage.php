<?php
class PropertySearchPage extends Page {

}

class PropertySearchPage_Controller extends Page_Controller {

public function index(SS_HTTPRequest $request) {
    $properties = Property::get()->limit(20);

    /*if($search = $request->getVar('Keywords')) {
	    $properties = $properties->filter(array(
	        'Title:PartialMatch' => $search             
	    ));
	}*/

	if($bedrooms = $request->getVar('Bedrooms')) {
        $properties = $properties->filter(array(
            'Bedrooms:GreaterThanOrEqual' => $bedrooms
        ));
    }

    if($bathrooms = $request->getVar('Bathrooms')) {
        $properties = $properties->filter(array(
            'Bathrooms:GreaterThanOrEqual' => $bathrooms
        ));
    }

    return array (
        'Results' => $properties
    );
}	

 public function PropertySearchForm() {
        $form = Form::create(
            $this,
            'PropertySearchForm',
            FieldList::create(
                DropdownField::create('CountryName','Country',Country::get()->map('CountryName','CountryName'))
	                ->setEmptyString('--Select a Country--'), 

	            DropdownField::create('DistrictName','District',District::get()->map('DistrictName','DistrictName'))
	                ->setEmptyString('--Select a District--'), 

	            DropdownField::create('SuburbName','Suburb',Suburb::get()->map('SuburbName','SuburbName'))
	                ->setEmptyString('--Select a Suburb--'),  

                DropdownField::create('Bedrooms') 
                	//->setEmptyString('-- any --')                  
                    ->setSource(ArrayLib::valuekey(range(1,5)))
                    ->addExtraClass('form-control'),
                
                DropdownField::create('Bathrooms') 
                	//->setEmptyString('-- any --')                 
                    ->setSource(ArrayLib::valuekey(range(1,5)))
                    ->addExtraClass('form-control')
                
                /*DropdownField::create('Bedrooms','Min')
                    ->setEmptyString('-- any --')
                    //->setSource($Bedrooms)
                    ->setSource(ArrayLib::valuekey(range(1,5)))
                    ->addExtraClass('form-control'),

                DropdownField::create('Bedrooms','Max')
                    ->setEmptyString('-- any --')
                    //->setSource($Bedrooms)
                    ->setSource(ArrayLib::valuekey(range(1,5)))
                    ->addExtraClass('form-control'),

                DropdownField::create('Bathrooms','Min')
                    ->setEmptyString('-- any --')
                    //->setSource($Bathrooms)
                    ->setSource(ArrayLib::valuekey(range(1,5)))
                    ->addExtraClass('form-control'),

                DropdownField::create('Bathrooms','Max')
                    ->setEmptyString('-- any --')
                    //->setSource($Bathrooms)
                    ->setSource(ArrayLib::valuekey(range(1,5)))
                    ->addExtraClass('form-control')*/
            ),
            FieldList::create(
                FormAction::create('doPropertySearch','Search')
                    ->addExtraClass('btn btn-default')
            )
        );
		
	    $form->setFormMethod('GET')
	         ->setFormAction($this->Link())
	         ->disableSecurityToken()
 			 ->loadDataFrom($this->request->getVars());

        return $form;
    }

}