<?php
class SimilarPropertiesAdmin extends ModelAdmin {

    private static $menu_title = 'Similar Properties';

    private static $url_segment = 'similar_properties';

    private static $managed_models = array (
        'SimilarProperties'
    );

    //private static $menu_icon = 'mysite/icons/property.png';
}