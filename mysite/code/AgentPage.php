<?php
class AgentPage extends Page{

}

class AgentPage_Controller extends Page_Controller{
	public function LastArticles( $count = 10 ){
		return ArtilcePage::get()
				->sort('Created','DESC')
				->limit($count);
	}

	public function FeaturedProperties() {
	    return Property::get()
		    ->filter(array(
		        'Agent_Name' => true
		    ))
		    ->limit(10);
	}
}