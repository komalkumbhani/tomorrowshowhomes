<?php
class SuburbAdmin extends ModelAdmin {

    private static $menu_title = 'Suburb';

    private static $url_segment = 'Suburb';

    private static $managed_models = array (
        'Suburb'
    );

    //private static $menu_icon = 'mysite/icons/property.png';
}