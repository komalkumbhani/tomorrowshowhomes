<?php
class Property extends DataObject {

    private static $db = array (
        'Title' => 'Varchar(255)',
        'FeaturedOnHomepage' => 'Boolean',
        'Description' => 'HTMLText',
        //'Price' => 'Currency',
        'Area' => 'Int',
        'Bedrooms' => 'Int',
        'Bathrooms' => 'Int',
        'Garages' =>'Int',
        'Address' => 'Varchar',
        'CountryName' => 'Varchar',
        'PostalCode' => 'Varchar',
        'AgentID' => 'Int',
        'Related_properties' => 'Varchar',
        'Lat' => 'Varchar',
        'Lng' => 'Varchar',
        'DistrictName' => 'Varchar',
        'SuburbName' => 'Varchar',
        'WorkingHours' => 'HTMLText',
        'JoomagEbookLink' => 'Varchar(500)',
        'VideoFile' => 'Varchar'
    );

    private static $has_one = array (
        'CompanyLogo' => 'Individual_SmallListImage',
        'PrimaryPhoto' => 'Image',
        'FloorPlanID1' =>'Image',
        'FloorPlanID2' =>'Image'
    );

    /*private static $many_many = array(
        'Images' => 'Image' 
    );*/

    private static $summary_fields = array (
        'Title' => 'Title',
        'FeaturedOnHomepage.Nice' => 'Featured?',
        //'Price.Nice' => 'Price',
        'Area' => 'Area (<span>m<sup>2</sup></span>)',
        'Bedrooms' => 'Bedrooms',
        'Bathrooms' => 'Bathrooms',
        'Garages' => 'Garages',
        'DistrictName' => 'District'
    );

    private static $searchable_fields = array (
        'Title',
        'CountryName',
        'FeaturedOnHomepage'
    );

    public function searchableFields() {
        return array (
            'Title' => array (
                'filter' => 'PartialMatchFilter',
                'title' => 'Title',
                'field' => 'TextField'
            ),
            'CountryName' => array (
                'filter' => 'ExactMatchFilter',
                'title' => 'Country Name',
                'field' => DropdownField::create('CountryName')
                                ->setSource(ArrayLib::valuekey(range(1,10)))
                                ->setEmptyString('-- Any Country --')
            ),
            'FeaturedOnHomepage' => array (
                'filter' => 'ExactMatchFilter',
                'title' => 'Only featured'              
            )
        );
    }

    /*public function updateCMSFields(FieldList $fields) {

        $fields->addFieldToTab('Root.Gallery', GalleryUploadField::create(
            'Images',
            ''
        ));
    }
    
    public function OrderedImages() {

        list($parentClass, $componentClass, $parentField, $componentField, $table) = $this->owner->many_many('Images');

        return $this->owner->getManyManyComponents(
            'Images',
            '',
            "\"{$table}\".\"SortOrder\" ASC"
        );
    }*/

    public function getCMSfields() {
        //$fields = parent::getCMSFields();
        $fields = FieldList::create(TabSet::create('Root'));
        $agentdata = Agent::get();
        
        $fields->addFieldsToTab('Root.Main', array(
            TextField::create('Title'),
            CheckboxField::create('FeaturedOnHomepage','Feature on homepage'),
            HTMLEditorField::create('Description'),
            //CurrencyField::create('Price','Price (per night)'),
            TextField::create('Area','Area (<span>m<sup>2</sup></span>)'),
            TextField::create('JoomagEbookLink'),
            DropdownField::create('Bedrooms')->setSource(ArrayLib::valuekey(range(1,10))),
            DropdownField::create('Bathrooms')->setSource(ArrayLib::valuekey(range(1,10))),
            DropdownField::create('Garages')->setSource(ArrayLib::valuekey(range(1,10))),                
            DropdownField::create('AgentID','Agent Name',Agent::get()->map('ID','Name'))->setEmptyString('--Select an Agent--'),
            HTMLEditorField::create('WorkingHours','Working Hours'),
            UploadField::create('CompanyLogo','Company Logo')
                    ->setAllowedExtensions(array('png','jpeg','jpg','gif'))
                    ->setFolderName('company-logo'),
            TextField::create('VideoFile')
        ));

        $fields->addFieldsToTab('Root.Address',array(
            TextareaField::create('Address'),
            DropdownField::create('CountryName','Country',Country::get()->map('ID','CountryName'))
                ->setEmptyString('--Select a Country--'), 
            DropdownField::create('DistrictName','District',District::get()->map('ID','DistrictName'))
                ->setEmptyString('--Select a District--'), 
            DropdownField::create('SuburbName','Suburb',Suburb::get()->map('ID','SuburbName'))
                ->setEmptyString('--Select a Suburb--'),
            TextField::create('PostalCode'),
            //LiteralField::Create('Map', '<iframe width="600" height="450" src="https://www.google.com/maps/embed/v1/view?zoom=11&center=-'.$this->Lat.','.$this->Lng.'&key='.GOOGLE_MAP_KEY.'"></iframe>')
        ));

        $fields->addFieldsToTab("Root.Address", array(
            // Create hidden latitude field
            TextField::create("Lat"),
            // Create hidden longitude field
            TextField::create("Lng"),
            // Create Google map field
            GoogleMapField::create("Map", array(
                "width" => "695px",
                "height" => "400px",                          // The height of the map element
                "heading" => "",                              // A heading in a <h4> tag to appear before the map
                "lng_field" => "Lat",                         // The ID of the longitude input element
                "lat_field" => "Lng",                         // The ID of the latitude input element
                "tab" => "Root_Location",                     // The ID of the tab that the map is in
                "address_field" => "Address",                 // The ID of the address field (if it exists)
                "map_zoom" => 10,                             // The initial zoom level of the map
                "start_lat" => "-36.8621432",                   // The initial latitude of the map marker
                "start_lng" => "174.5845903"                    // The initial longitude of the map marker
            ))
        ));
     
        $fields->addFieldsToTab('Root.Related properties',array(
            ListboxField::create('Related_properties','Related properties',
            Property::get()->map()->toArray())->setMultiple(true)->setEmptyString('--Select a Related Property--')));

        $fields->addFieldsToTab('Root.Gallery', $upload = UploadField::create('PrimaryPhoto','Primary photo'));
        $upload->getValidator()->setAllowedExtensions(array('png','jpeg','jpg','gif'));
        $upload->setFolderName('property-photos');

        $fields->addFieldsToTab('Root.Floor Plan', $FloorPlanID1 = UploadField::create('FloorPlanID1','Floor Plan 1'));
        $FloorPlanID1->getValidator()->setAllowedExtensions(array('png','jpeg','jpg','gif'));
        $FloorPlanID1->setFolderName('FloorPlan-photos');

        $fields->addFieldsToTab('Root.Floor Plan', $FloorPlanID2 = UploadField::create('FloorPlanID2','Floor Plan 2'));
        $FloorPlanID2->getValidator()->setAllowedExtensions(array('png','jpeg','jpg','gif'));
        $FloorPlanID2->setFolderName('FloorPlan-photos');

        $fields->addFieldToTab('Root.Gallery', $galleryField = GalleryUploadField::create('Images','Upload images'));
        $galleryField->getValidator()->setAllowedExtensions(array('png','jpeg','jpg','gif'));
        $galleryField->setFolderName('Gallery-photos');

        return $fields;
    }
    function onBeforeWrite() {
        // call parent first
        parent::onBeforeWrite();
        /*echo "<pre>";
        print_r($_REQUEST);
        print_r($this);
        exit;*/
        // check if FeaturedOnHomepage is true
        if( $this->FeaturedOnHomepage ){
            // if yes, make sure no other FeaturedOnHomepage is Feachured.
            // we use a raw sql query here to set all FeaturedOnHomepage to 0
            DB::query('UPDATE ' . 'Property' . ' SET FeaturedOnHomepage=0');

            DB::query('UPDATE ' . 'Property' . ' SET FeaturedOnHomepage=1 WHERE ID=' . $this->ID);
        }
    }
}

class Individual_SmallListImage extends Image {
    function generateWebsiteThumbnail($gd){
    return $gd->resizeByHeight(83);
    }
}