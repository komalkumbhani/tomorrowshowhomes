<nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
    <ul class="nav navbar-nav">
		<% loop $Menu(1) %>
			<li class="$LinkingMode"><a href="$Link" title="$Title.XML">$MenuTitle.XML</a></li>
		<% end_loop %>
			<li><a href="#basic" class="basic_open"  data-popup-ordinal="0" id="open_63768381">Contact</a></li>
	</ul>
</nav>
<!-- /.navbar collapse-->